using System;
using System.Collections.Generic;

namespace RoleCatalogueDao
{
    public class OrgUnit
    {
        public string Uuid { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public string ParentUuuid { get; set; }
    }
}
