﻿using System;
using System.Linq;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using RoleCatalogueDao;

namespace RoleCatalogueDao
{
    public class OrgUnitDao
    {
        private string connectionString = null;

        public OrgUnitDao()
        {
            connectionString = System.Environment.GetEnvironmentVariable("RoleCatalogueDBConnectionString");
        }

        public List<OrgUnit> GetAllModifiedSince(DateTime? since, DateTime until)
        {
            var orgUnits = new List<OrgUnit>();

            if (since == null)
            {
                since = DateTime.MinValue;
            }

            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                connection.Open();

                using (MySqlCommand command = new MySqlCommand("SELECT * FROM ous WHERE last_updated > @since AND last_updated < @until", connection))
                {
                    command.Parameters.Add(new MySqlParameter("@since", since));
                    command.Parameters.Add(new MySqlParameter("@until", until));

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var ou = new OrgUnit();
                                ou.Uuid = GetValue(reader, "uuid");
                                ou.Name = GetValue(reader, "name");
                                ou.Active = GetBoolValue(reader, "active");
                                ou.ParentUuuid = GetValue(reader, "parent_uuid");

                                orgUnits.Add(ou);
                            }
                        }
                    }
                }
            }

            return orgUnits;
        }

        private string GetValue(MySqlDataReader reader, string key)
        {
            if (reader[key] is DBNull)
            {
                return null;
            }

            return (string)reader[key];
        }

        private bool GetBoolValue(MySqlDataReader reader, string key)
        {
            if (reader[key] is DBNull)
            {
                return false;
            }

            return (bool)reader[key];
        }
    }
}
