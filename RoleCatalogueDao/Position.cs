using System;
using System.Collections.Generic;

namespace RoleCatalogueDao
{
    public class Position
    {
        public string Title { get; set; }
        public string OrgUnitUuid { get; set; }
    }
}
