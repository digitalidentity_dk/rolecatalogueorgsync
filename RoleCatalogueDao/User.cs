using System;
using System.Collections.Generic;

namespace RoleCatalogueDao
{
    public class User
    {
        public string Uuid { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public List<Position> Positions { get; set; }
    }
}
