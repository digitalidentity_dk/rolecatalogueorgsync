﻿using System;
using System.Linq;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using RoleCatalogueDao;

namespace RoleCatalogueDao
{
    public class UserDao
    {
        private string connectionString = null;

        public UserDao()
        {
            connectionString = System.Environment.GetEnvironmentVariable("RoleCatalogueDBConnectionString");
        }

        public List<User> GetAllModifiedSince(DateTime? since, DateTime until)
        {
            var users = new List<User>();

            if (since == null)
            {
                since = DateTime.MinValue;
            }

            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                connection.Open();

                using (MySqlCommand command = new MySqlCommand("SELECT * FROM users WHERE last_updated > @since AND last_updated < @until", connection))
                {
                    command.Parameters.Add(new MySqlParameter("@since", since));
                    command.Parameters.Add(new MySqlParameter("@until", until));

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var user = new User();
                                user.Positions = new List<Position>();
                                user.Uuid = GetValue(reader, "uuid");
                                user.UserId = GetValue(reader, "user_id");
                                user.Name = GetValue(reader, "name");
                                user.Active = GetBoolValue(reader, "active");
                                user.Email = GetValue(reader, "email");
                                user.Phone = GetValue(reader, "phone");

                                users.Add(user);
                            }
                        }
                    }
                }

                foreach (var user in users)
                {
                    using (MySqlCommand posCommand = new MySqlCommand("SELECT * FROM positions WHERE user_uuid = @uuid", connection))
                    {
                        posCommand.Parameters.Add(new MySqlParameter("@uuid", user.Uuid));

                        using (MySqlDataReader posReader = posCommand.ExecuteReader())
                        {
                            while (posReader.Read())
                            {
                                Position position = new Position();
                                position.Title = GetValue(posReader, "name");
                                position.OrgUnitUuid = GetValue(posReader, "ou_uuid");

                                user.Positions.Add(position);
                            }
                        }
                    }
                }
            }

            return users;
        }

        private string GetValue(MySqlDataReader reader, string key)
        {
            if (reader[key] is DBNull)
            {
                return null;
            }

            return (string)reader[key];
        }

        private bool GetBoolValue(MySqlDataReader reader, string key)
        {
            if (reader[key] is DBNull)
            {
                return false;
            }

            return (bool)reader[key];
        }
    }
}
