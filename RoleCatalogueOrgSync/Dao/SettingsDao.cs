using System;
using System.Linq;
using MySql.Data.MySqlClient;

namespace RoleCatalogueOrgSync
{
    public class SettingsDao
    {
        private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string connectionString = null;

        public SettingsDao()
        {
            connectionString = System.Environment.GetEnvironmentVariable("DBConnectionString");
            CreateTableIfNotExists();
        }

        public DateTime GetLastRun(string cvr)
        {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                connection.Open();

                using (MySqlCommand command = new MySqlCommand("SELECT * FROM last_run_v2 WHERE cvr = @cvr", connection))
                {
                    command.Parameters.Add(new MySqlParameter("@cvr", cvr));

                    using (MySqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            if (reader.Read())
                            {
                                if (!(reader["last_run_timestamp"] is DBNull))
                                {
                                    return (DateTime) reader["last_run_timestamp"];
                                }
                            }
                        }
                    }
                }
            }

            // if we cannot read, make sure a value is set for later reference
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                connection.Open();

                using (MySqlCommand command = new MySqlCommand("INSERT INTO last_run_v2 (cvr, last_run_timestamp) VALUES (@cvr, @lastrun)", connection))
                {
                    command.Parameters.Add(new MySqlParameter("@lastrun", DateTime.MinValue));
                    command.Parameters.Add(new MySqlParameter("@cvr", cvr));

                    command.ExecuteNonQuery();
                }
            }

            return DateTime.MinValue;
        }

        public void SetLastRun(String cvr, DateTime lastRun)
        {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                connection.Open();

                using (MySqlCommand command = new MySqlCommand("UPDATE last_run_v2 SET last_run_timestamp = @lastrun WHERE cvr = @cvr", connection))
                {
                    command.Parameters.Add(new MySqlParameter("@lastrun", lastRun));
                    command.Parameters.Add(new MySqlParameter("@cvr", cvr));

                    command.ExecuteNonQuery();
                }
            }
        }

        private void CreateTableIfNotExists()
        {
            using (MySqlConnection connection = new MySqlConnection(connectionString))
            {
                connection.Open();

                int value = -1;
                using (MySqlCommand command = new MySqlCommand("CREATE TABLE IF NOT EXISTS last_run_v2 (cvr varchar(8), last_run_timestamp DATETIME NULL)", connection))
                {
                    value = command.ExecuteNonQuery();
                }
            }
        }
    }
}
