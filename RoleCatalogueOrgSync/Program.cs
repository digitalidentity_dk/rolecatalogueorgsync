﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Organisation.BusinessLayer;

namespace RoleCatalogueOrgSync
{
    public class Program
    {
        private static string port = "5001";

        public static void Main(string[] args)
        {
            // Initialize BusinessLayer
            Initializer.Init();

            // Initialize our RC Sync Job
            RCSyncJobRunner.InitAsync();

            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseUrls("http://0.0.0.0:" + port)
                .Build();
    }
}
