using System;
using System.Collections.Generic;

namespace RoleCatalogueOrgSync
{
    internal class RCSyncJob
    {
        private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static SettingsDao settingsDao = new SettingsDao();
        private static RoleCatalogueDao.UserDao userDao = new RoleCatalogueDao.UserDao();
        private static RoleCatalogueDao.OrgUnitDao orgUnitDao = new RoleCatalogueDao.OrgUnitDao();
        private static Organisation.SchedulingLayer.UserDao schedulerUserDao = new Organisation.SchedulingLayer.UserDao();
        private static Organisation.SchedulingLayer.OrgUnitDao schedulerOrgUnitDao = new Organisation.SchedulingLayer.OrgUnitDao();
        private static string cvr = System.Environment.GetEnvironmentVariable("Cvr");

        public static void Run()
        {
            log.Debug("Running our RC SyncJob!");

            DateTime since = settingsDao.GetLastRun(cvr);
            DateTime until = DateTime.Now.AddMinutes(-5);
            
            var users = userDao.GetAllModifiedSince(since, until);
            var orgUnits = orgUnitDao.GetAllModifiedSince(since, until);

            if (users.Count > 0 || orgUnits.Count > 0)
            {
                log.Info("Found " + users.Count + " users and " + orgUnits.Count + " orgUnits that has been changed since last run");
            }

            try
            {
                foreach (var orgUnit in orgUnits)
                {
                    var reg = new Organisation.SchedulingLayer.OrgUnitRegistrationExtended();
                    reg.Cvr = cvr;
                    reg.Uuid = orgUnit.Uuid;
                    reg.Type = Organisation.BusinessLayer.DTO.Registration.OrgUnitRegistration.OrgUnitType.DEPARTMENT;
                    reg.Timestamp = DateTime.Now;

                    if (orgUnit.Active == true)
                    {
                        reg.Operation = Organisation.SchedulingLayer.OperationType.UPDATE;
                        reg.Name = orgUnit.Name;

                        if (!string.IsNullOrEmpty(orgUnit.ParentUuuid))
                        {
                            reg.ParentOrgUnitUuid = orgUnit.ParentUuuid;
                        }
                    }
                    else
                    {
                        reg.Operation = Organisation.SchedulingLayer.OperationType.DELETE;
                    }

                    schedulerOrgUnitDao.Save(reg, reg.Operation, cvr);
                }

                foreach (var user in users)
                {
                    var reg = new Organisation.SchedulingLayer.UserRegistrationExtended();
                    reg.Cvr = cvr;
                    reg.Uuid = user.Uuid;
                    reg.Timestamp = DateTime.Now;

                    if (user.Active == true)
                    {
                        reg.Operation = Organisation.SchedulingLayer.OperationType.UPDATE;
                        reg.UserId = user.UserId;
                        reg.ShortKey = user.UserId;
                        reg.Person.Name = user.Name;
                        reg.Positions = new List<Organisation.BusinessLayer.DTO.Registration.Position>();

                        if (user.Positions != null && user.Positions.Count > 0)
                        {
                            foreach (var position in user.Positions)
                            {
                                var pos = new Organisation.BusinessLayer.DTO.Registration.Position();
                                pos.Name = position.Title;
                                pos.OrgUnitUuid = position.OrgUnitUuid;

                                reg.Positions.Add(pos);
                            }
                        }

                        if (!string.IsNullOrEmpty(user.Phone))
                        {
                            reg.PhoneNumber = user.Phone;
                        }

                        if (!string.IsNullOrEmpty(user.Email))
                        {
                            reg.Email = user.Email;
                        }
                    }
                    else
                    {
                        reg.Operation = Organisation.SchedulingLayer.OperationType.DELETE;
                    }

                    schedulerUserDao.Save(reg, reg.Operation, cvr);
                }

                settingsDao.SetLastRun(cvr, until);
            }
            catch (Exception ex)
            {
                log.Error("Failed to synchronize tables", ex);
            }
        }
    }
}
