#!/bin/bash
OUTPUT="$(aws ecr get-login --region eu-west-1 --no-include-email)"
${OUTPUT}

(cd ..;./release.sh)

docker build -t rolecatalogeorgsync:latest .
docker tag rolecatalogeorgsync:latest 711926434486.dkr.ecr.eu-west-1.amazonaws.com/rolecatalogeorgsync:$(date +'%Y-%m-%d')
docker push 711926434486.dkr.ecr.eu-west-1.amazonaws.com/rolecatalogeorgsync:$(date +'%Y-%m-%d')

rm -Rf release/
